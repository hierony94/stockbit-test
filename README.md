# stockbit-test

## Pre-requisites
- Terraform installed v0.12.28
- Docker Engine 19.03.12

## About this Project
This project contain is a simple project regarding Terraform and CI/CD pipeline. tf folder contain terraform HCL to create the infrastructure.
And in the root folder we have files related to CI/CD like Dockerfile to create the Docker image and gitlab-ci.yaml to run the deployment the image to EC2 server.

## Notes regarding the CI/CD
 - I added 4 environment Variables to the CI/CD pipelines, they are
    1. $SSH_PRIVATE_KEY, Key that used to ssh to the remote server
    2. $CI_REGISTRY, Registry url
    3. $CI_REGISTRY_USER, User to the Docker registry hub
    4. CI_REGISTRY_PASSWORD, User's password 
    
 - In before_script section, we need this for the SSH deployment to remote machine (https://docs.gitlab.com/ee/ci/ssh_keys/).
 - We also need to add the public SSH key to the remote machine.
 - In build stage, I build the image from the Dockerfile and push it to Dockerhub public registry. I tagged every built image with latest and Commit hash
   I create this, so we have different image for each successful built image.
 - In deploy stage, I ssh to the remote server and run the container from the images that I have pushed to the registry. I used my server (bacapesat.com) on AWS to try this.

 ## Things that can be improved.
 - Instead deploy it to the EC2, we can deploy it to ECS (Elastic Container Service). ECS is designed for running Docker container, Some of the big Advantage of ECS over ec2 is you do not need to worry about container management, scalability and availability, ECS will take care of it. It also provide ECR which is like docker registry but it's private and with in-network.