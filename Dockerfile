FROM nginx:stable

COPY hello.txt /var/www/

CMD ["nginx" "-g" "daemon off;"]
