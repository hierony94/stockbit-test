resource "aws_launch_template" "asg_template" {
  name_prefix   = "Stockbit-Test"
  image_id      = "ami-0bd65d371156c9cf9" // I take from public images of Ubuntu18, Prefer to use own AMI tho 
  instance_type = "t2.medium"
}

resource "aws_autoscaling_group" "asg" {
  availability_zones  = ["ap-southeast-1a"]
  desired_capacity    = 2
  min_size            = 2
  max_size            = 5
  vpc_zone_identifier = ["${aws_subnet.private.id}"]

  launch_template {
    id = "${aws_launch_template.asg_template.id}"
  }
}

resource "aws_autoscaling_policy" "asg_policy" {
  name               = "asg_policyt"
  scaling_adjustment = 1
  adjustment_type    = "ChangeInCapacity"
  cooldown           = 300

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 45.0
  }
  autoscaling_group_name = "${aws_autoscaling_group.asg.name}"
}
